/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   draw_map_utils.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: anramire <anramire@student.42malaga.com>   +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/06/15 19:39:21 by anramire          #+#    #+#             */
/*   Updated: 2022/06/17 02:16:32 by anramire         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/fdf.h"

int	get_highest(t_map *map)
{
	int	highest;
	int	i;
	int	j;

	i = 0;
	highest = 0;
	while (i < map->dots_y)
	{
		j = 0;
		while (j < map->dots_x)
		{
			if (map->points[i][j] > highest)
				highest = map->points[i][j];
			j++;
		}
		i++;
	}
	return (highest);
}

void	liberate_map(t_map *map)
{
	int	i;

	i = 0;
	while (i < map->dots_y)
	{
		free(map->points[i]);
		i++;
	}
	free(map->points);
}

void	draw_horizontal_lines(t_draw_map *ut, int i, int j, mlx_image_t *g_img)
{
	t_point	*p1;
	t_point	*p2;

	p1 = get_point((j * ut->increment_x), (i * ut->increment_y));
	p1->z = ut->map->points[i][j];
	p1 = get_projection(p1, ut->highest);
	p1->x = p1->x + ut->des_x;
	p1->y = p1->y + ut->des_y;
	p2 = get_point((j + 1) * ut->increment_x, (i * ut->increment_y));
	p2->z = ut->map->points[i][j + 1];
	p2 = get_projection(p2, ut->highest);
	p2->x = p2->x + ut->des_x;
	p2->y = p2->y + ut->des_y;
	draw_line(p1, p2, g_img);
	free(p1);
	free(p2);
}

void	draw_vertical_lines(t_draw_map *ut, int i, int j, mlx_image_t *g_img)
{
	t_point	*p1;
	t_point	*p2;

	p1 = get_point((j * ut->increment_x), (i * ut->increment_y));
	p1->z = ut->map->points[i][j];
	p1 = get_projection(p1, ut->highest);
	p1->x = p1->x + ut->des_x;
	p1->y = p1->y + ut->des_y;
	p2 = get_point((j * ut->increment_x), ((i + 1) * ut->increment_y));
	p2->z = ut->map->points[i + 1][j];
	p2 = get_projection(p2, ut->highest);
	p2->x = p2->x + ut->des_x;
	p2->y = p2->y + ut->des_y;
	draw_line(p1, p2, g_img);
	free(p1);
	free(p2);
}
