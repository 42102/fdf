/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_utils.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: anramire <anramire@student.42malaga.com>   +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/06/09 21:17:34 by anramire          #+#    #+#             */
/*   Updated: 2022/06/17 02:16:52 by anramire         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/fdf.h"

void	ft_swap(int *a, int *b)
{
	int	aux;

	aux = (*a);
	(*a) = (*b);
	(*b) = aux;
}

int	ft_absolute(int n)
{
	if (n < 0)
		return (-n);
	return (n);
}
