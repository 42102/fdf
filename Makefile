#------------------------------- Makefile ----------------------------------

SRC = fdf.c operations.c shapes.c ft_utils.c read_file.c draw_map.c\
	  draw_map_utils.c read_file_utils.c
SRC1 = $(addprefix src/, $(SRC))
OBJS = $(SRC1:.c=.o)
CC = gcc
CFLAGS = -Wall -Werror -Wextra
NAME = fdf
MLXLIBS = -Lmlx -lmlx42 -L ~/.brew/opt/glfw/lib -l glfw
GNLLIB = -Lft_get_next_line -lget_next_line
LIBFT = -Llibft -lft
INCLUDES = includes/fdf.h

%.o: %.c
	$(CC) $(CFLAGS) -o $@ -c $<
all: $(NAME)
re: fclean all

$(NAME): $(OBJS) $(INCLUDES)
	make -C ft_get_next_line
	make -C libft
	$(MAKE) -C mlx
	$(CC) $(OBJS) $(LIBFT) $(GNLLIB) $(MLXLIBS) -o $(NAME)

clean:
	rm -rf $(OBJS)
	make clean -C ft_get_next_line
	make clean -C libft
	make clean -C mlx

fclean: clean
	rm -rf $(NAME)
	make fclean -C ft_get_next_line
	make fclean -C libft
	make fclean -C mlx

.PHONY: clean fclean all re
