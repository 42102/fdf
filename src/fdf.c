/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fdf.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: anramire <anramire@student.42malaga.com>   +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/06/08 18:43:03 by anramire          #+#    #+#             */
/*   Updated: 2022/06/17 03:11:26 by anramire         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/fdf.h"

void	hook(mlx_key_data_t k, void *param);

int	main(int argc, char *argv[])
{
	t_map		map;
	mlx_t		*mlx;
	mlx_image_t	*g_img;

	if (argc == 2)
	{
		get_map(argv[1], &map);
		mlx = mlx_init(WIDTH, HEIGHT, "MLX42", true);
		if (!mlx)
			exit(EXIT_FAILURE);
		g_img = mlx_new_image(mlx, WIDTH, HEIGHT);
		draw_map(&map, g_img);
		mlx_image_to_window(mlx, g_img, 0, 0);
		mlx_key_hook(mlx, hook, mlx);
		mlx_loop(mlx);
		mlx_delete_image(mlx, g_img);
		mlx_terminate(mlx);
		liberate_map(&map);
	}
	return (EXIT_SUCCESS);
}

void	hook(mlx_key_data_t k, void *param)
{
	if (k.key == MLX_KEY_ESCAPE)
		mlx_close_window((mlx_t *)param);
}
