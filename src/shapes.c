/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   shapes.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: anramire <anramire@student.42malaga.com>   +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/06/09 00:31:42 by anramire          #+#    #+#             */
/*   Updated: 2022/06/17 02:17:44 by anramire         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/fdf.h"

typedef struct s_aux_map{
	int		error;
	int		e2;
	t_point	p;
	t_point	d;
	t_point	s;
}t_aux_map;

static void	paint(t_point *p1, t_point *p2, t_point *p, mlx_image_t *g_img);
static void	draw_loop(t_aux_map *aux, t_point *p1,
				t_point *p2, mlx_image_t *g_img);

void	draw_line(t_point *p1, t_point *p2, mlx_image_t *g_img)
{
	t_aux_map	aux;

	aux.d.x = ft_absolute(p2->x - p1->x);
	aux.p.x = p1->x;
	aux.p.y = p1->y;
	if (aux.p.x < p2->x)
		aux.s.x = 1;
	else
		aux.s.x = -1;
	aux.d.y = -1 * ft_absolute(p2->y - aux.p.y);
	if (aux.p.y < p2->y)
		aux.s.y = 1;
	else
		aux.s.y = -1;
	aux.error = aux.d.x + aux.d.y;
	draw_loop(&aux, p1, p2, g_img);
}

static void	paint(t_point *p1, t_point *p2, t_point *p, mlx_image_t *g_img)
{
	if (p2->z > 60 || (p1->z > 60))
		mlx_put_pixel(g_img, p->x, p->y, 0x944B01FF);
	else if (p2->z > 30 || (p1->z > 30))
		mlx_put_pixel(g_img, p->x, p->y, 0xAD8500FF);
	else if (p2->z > 0 || (p1->z > 0))
		mlx_put_pixel(g_img, p->x, p->y, 0x00FF00FF);
	else if (p2->z > -100 || (p1->z > -100))
		mlx_put_pixel(g_img, p->x, p->y, 0x03B1FCFF);
	else
		mlx_put_pixel(g_img, p->x, p->y, 0x0269DEFF);
}

static void	draw_loop(t_aux_map *aux, t_point *p1,
				t_point *p2, mlx_image_t *g_img)
{
	while (1)
	{	
		paint(p1, p2, &aux->p, g_img);
		if ((aux->p.x == p2->x) && (aux->p.y == p2->y))
			break ;
		aux->e2 = 2 * aux->error;
		if (aux->e2 >= aux->d.y)
		{
			if (aux->p.x == p2->x)
				break ;
			aux->error = aux->error + aux->d.y;
			aux->p.x = aux->p.x + aux->s.x;
		}
		if (aux->e2 <= aux->d.x)
		{
			if (aux->p.y == p2->y)
				break ;
			aux->error = aux->error + aux->d.x;
			aux->p.y = aux->p.y + aux->s.y;
		}
	}
}
