/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   read_file_utils.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: anramire <anramire@student.42malaga.com>   +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/06/17 01:08:03 by anramire          #+#    #+#             */
/*   Updated: 2022/06/17 02:17:33 by anramire         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/fdf.h"

void	get_map_loop(t_map *map, int fd)
{
	int		i;
	int		j;
	char	*str;
	char	**strs;
	int		k;

	i = 0;
	str = get_next_line(fd);
	while (i < map->dots_y)
	{
		strs = ft_split(str, ' ');
		free(str);
		j = 0;
		while (j < map->dots_x)
		{
			map->points[i][j] = ft_atoi(strs[j]);
			j++;
		}
		k = 0;
		while (strs[k] != NULL)
			free(strs[k++]);
		free(strs);
		i++;
		str = get_next_line(fd);
	}
}

void	get_points_from_str_loop(t_map *map, int fd)
{
	char	*str;
	int		k;
	char	**strs;
	char	**ret;

	str = get_next_line(fd);
	while (str != NULL)
	{
		strs = ft_split(str, ' ');
		ret = strs;
		map->dots_x = 0;
		while ((*strs) != NULL)
		{
			strs++;
			(map->dots_x)++;
		}
		k = 0;
		while (ret[k] != NULL)
			free(ret[k++]);
		free(ret);
		free(str);
		str = get_next_line(fd);
		(map->dots_y)++;
	}
}
