/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   draw_map.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: anramire <anramire@student.42malaga.com>   +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/06/15 00:00:16 by anramire          #+#    #+#             */
/*   Updated: 2022/06/17 02:16:18 by anramire         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/fdf.h"

int		get_des_y(t_map *map, int inc_x, int inc_y, int high);
int		get_des_x(t_map *map, int inc_x, int inc_y, int high);
void	draw_map_loop(t_draw_map *utils, mlx_image_t *g_img);

void	draw_map(t_map *map, mlx_image_t *g_img)
{
	int			inc_x;
	int			inc_y;
	t_draw_map	utils_map;

	inc_x = 1080 / (map->dots_x - 1);
	inc_y = 720 / (map->dots_y - 1);
	utils_map.highest = get_highest(map);
	utils_map.des_y = get_des_y(map, inc_x, inc_y, utils_map.highest);
	utils_map.des_x = get_des_x(map, inc_x, inc_y, utils_map.highest);
	utils_map.increment_x = inc_x;
	utils_map.increment_y = inc_y;
	utils_map.map = map;
	draw_map_loop(&utils_map, g_img);
}

t_point	*get_projection(t_point *point, int high)
{
	t_point	*projection;
	float	u;
	float	v;
	float	factor;

	factor = 50 / (float)high;
	projection = (t_point *)malloc(sizeof(t_point));
	u = (point->x - point->y) * cos(0.523599f);
	v = (point->x + point->y) * sin(0.523599f) +(-(point->z) * factor);
	projection->x = (int)round(u);
	projection->y = (int)round(v);
	projection->z = point->z;
	free(point);
	return (projection);
}

int	get_des_y(t_map *map, int inc_x, int inc_y, int high)
{
	t_point	*p1;
	t_point	*p2;
	float	des_y;

	p1 = get_point(0, 0);
	p2 = get_point((map->dots_x - 1) * inc_x, (map->dots_y - 1) * inc_y);
	p1->z = map->points[0][0];
	p2->z = map->points[map->dots_y - 1][map->dots_x - 1];
	p1 = get_projection(p1, high);
	p2 = get_projection(p2, high);
	high = p2->y - p1->y;
	des_y = 1500 - high;
	des_y = (des_y / 2);
	des_y = round(des_y);
	free(p1);
	free(p2);
	return ((int)des_y);
}

int	get_des_x(t_map *map, int inc_x, int inc_y, int high)
{
	t_point	*p1;
	t_point	*p2;
	int		des_x;
	int		width;

	p1 = get_point(0, (map->dots_y - 1) * inc_y);
	p2 = get_point((map->dots_x - 1) * inc_x, 0);
	p1->z = map->points[0][0];
	p2->z = map->points[map->dots_y - 1][map->dots_x - 1];
	p1 = get_projection(p1, high);
	p2 = get_projection(p2, high);
	if (p1->x < 0)
	{
		p1->x = ft_absolute(p1->x);
		p2->x += p1->x;
	}
	width = p2->x - p1->x;
	des_x = 1500 - width;
	des_x = (des_x / 2);
	des_x += p1->x;
	free(p1);
	free(p2);
	return (des_x);
}

void	draw_map_loop(t_draw_map *utils, mlx_image_t *g_img)
{
	int		i;
	int		j;

	i = 0;
	while (i < utils->map->dots_y)
	{
		j = 0;
		while (j < utils->map->dots_x)
		{
			if (j < utils->map->dots_x - 1)
				draw_horizontal_lines(utils, i, j, g_img);
			if (i < utils->map->dots_y - 1)
				draw_vertical_lines(utils, i, j, g_img);
			j++;
		}
		i++;
	}
}
