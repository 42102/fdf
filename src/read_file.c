/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   read_file.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: anramire <anramire@student.42malaga.com>   +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/06/14 19:05:28 by anramire          #+#    #+#             */
/*   Updated: 2022/06/17 02:17:18 by anramire         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/fdf.h"

void	get_points_from_str(char *path, t_map *map)
{
	size_t	fd;

	fd = open(path, O_RDONLY, 0444);
	if (fd < 0)
	{
		perror("Error opening fd: ");
		exit(-1);
	}
	(map->dots_y) = 0;
	get_points_from_str_loop(map, fd);
	if (close(fd) < 0)
	{
		perror("Error closing file: ");
		exit(EXIT_FAILURE);
	}
}

void	allocate_map(char *path, t_map *map)
{
	int	i;

	get_points_from_str(path, map);
	map->points = (int **)malloc(map->dots_y * sizeof(int *));
	i = 0;
	while (i < map->dots_y)
	{
		map->points[i] = (int *)malloc(map->dots_x * sizeof(int));
		i++;
	}
}

void	get_map(char *path, t_map *map)
{
	size_t	fd;

	fd = open(path, O_RDONLY, 0444);
	if (fd < 0)
	{
		perror("Error opening fd: ");
		exit(-1);
	}
	allocate_map(path, map);
	get_map_loop(map, fd);
	if (close(fd) < 0)
	{
		perror("Error al cerrar archivo: ");
		exit(EXIT_FAILURE);
	}
}
