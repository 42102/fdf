/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fdf.h                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: anramire <anramire@student.42malaga.com>   +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/06/08 23:56:15 by anramire          #+#    #+#             */
/*   Updated: 2022/06/17 02:08:13 by anramire         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FDF_H
# define FDF_H
# include <math.h>
# include <stdlib.h>
# include <stdio.h>
# include <unistd.h>
# include <fcntl.h>
# include "../ft_get_next_line/get_next_line.h"
# include "../libft/libft.h"
# include "../mlx/include/MLX42/MLX42.h"
# define WIDTH 1920
# define HEIGHT 1500

typedef struct s_point{
	int	x;
	int	y;
	int	z;
}	t_point;

typedef struct s_vector{
	int	d1;
	int	d2;

}	t_vector;

typedef struct s_map{
	int	dots_x;
	int	dots_y;
	int	**points;
}	t_map;

typedef struct s_draw_map{
	int		increment_x;
	int		increment_y;
	int		des_x;
	int		des_y;
	int		highest;
	t_map	*map;
}t_draw_map;

t_point		*get_point(int x, int y);
t_vector	*get_vector(t_point *p1, t_point *p2);
void		draw_line(t_point *p1, t_point *p2, mlx_image_t *g_img);

//utils
void		ft_swap(int *a, int *b);
int			ft_absolute(int n);
int			get_highest(t_map *map);
void		liberate_map(t_map *map);
void		draw_horizontal_lines(t_draw_map *ut, int i, int j,
				mlx_image_t *g_img);
void		draw_vertical_lines(t_draw_map *ut, int i, int j,
				mlx_image_t *g_img);
t_point		*get_projection(t_point *point, int high);

//read_file

void		get_map(char *path, t_map *map);
void		get_map_loop(t_map *map, int fd);
void		get_points_from_str_loop(t_map *map, int fd);

//draw_map
void		draw_map(t_map *map, mlx_image_t *g_img);
#endif
